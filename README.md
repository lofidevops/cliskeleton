# Command Line Skeleton

Use this skeleton to quickstart a command-line utility. See ADOPT.md for details.

TODO: https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html

## Usage

```
$ skeleton --help
Usage: skeleton [OPTIONS]

  Generic greeting.

Options:
  --help  Show this message and exit.
```

## Development environment

### Setup

```
git checkout <path>
cd <folder>
pipenv install -e . --dev
pipenv shell
```

You can use a text editor, or any IDE that supports virtualenv / pipenv.

For pipenv details see <https://pipenv.pypa.io/en/latest/>

### Toolchain

```
$ make help
clean    Remove all build artefacts
test     Run tests and generate coverage report
lint     Run linters, including modifications
build    Clean, test, lint, then generate new build artefacts
upload   Upload build artefacts
```

If a local build succeeds, your changes are ready for submission. Otherwise, you
need to fix, commit and validate again.

You can invoke these tasks in any CI/CD pipeline with `pipenv run make X`.

## Sharing and contributions

Command Line Skeleton \
<https://example.com> \
Copyright 1899 AA Lovelace \
SPDX-License-Identifier: CC0-1.0

Shared under CC0-1.0. We adhere to the Contributor Covenant 2.0 without modification, and certify origin per DCO 1.1
with a signed-off-by line. Contributions under the same terms are welcome.

Submit security and conduct issues as private tickets. Sign commits with `git -s`. For a software bill of materials run
`reuse spdx`. For more details see LICENSES, CONDUCT and DCO.
