# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: Copyright 1899 AA Lovelace

import click


def greeting():
    """Return generic greeting."""
    return "Hello world"


@click.command()
def invoke():
    """Generic greeting."""
    print(greeting())
