# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: Copyright 1899 AA Lovelace

.PHONY: build
build: clean test lint
	python setup.py sdist bdist_wheel

.PHONY: clean
clean:
	rm -rf .pytest_cache
	rm -rf *.egg-info
	rm -f .coverage
	rm -rf dist
	rm -rf build
	pipenv install --dev -e .

.PHONY: test
test:
	coverage run -m pytest
	coverage report > coverage.txt

.PHONY: lint
lint:
	pipenv check
	black .
	reuse lint
	git diff --exit-code

.PHONY: upload
upload:
	twine upload dist/*

.PHONY: help
help:
	@echo "clean \t Remove all build artefacts"
	@echo "test \t Run tests and generate coverage report"
	@echo "lint \t Run linters, including modifications"
	@echo "build \t Clean, test, lint, then generate new build artefacts"
	@echo "upload \t Upload build artefacts"
