# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: Copyright 1899 AA Lovelace
import skeleton.bones


def test_greeting():
    assert "Hello world" == skeleton.bones.greeting()
