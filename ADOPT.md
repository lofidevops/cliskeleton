# How to adopt

This skeleton is intended to be re-licensed and completely overhauled, allowing you
to adopt and optionally adapt the toolchain. To do so:

1. Clone this folder (except this file).

2. Search and replace the following strings:

    ```
    Application slug (distro package name, CLI invocation, parent folder):
    cliskeleton

    Application title:
    Command Line Skeleton

    One-line application description:
    Use this skeleton to quickstart a command-line utility. See ADOPT.md for details.

    Project URL:
    https://example.com

    Copyright year:
    1899

    Author name/copyright:
    AA Lovelace

    Author email address:
    example@example.com

    SPDX license identifier:
    SPDX-License-Identifier: CC0-1.0

    Primary Python package:
    skeleton

    Primary Python module:
    bones
    ```

3. Fix license issues with `pipenv run reuse`.

4. Correct "License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication" in
   `setup.cfg`. See <https://pypi.org/classifiers/>

5. Fix version in `setup.cfg`

6. Run `make build` to check everything still works.

7. Happy hacking!

For examples of projects based on this skeleton, see:

* <https://gitlab.com/lofidevops/checkyoself>
* <https://gitlab.com/lofidevops/uptimecurl>

If you'd prefer a richer templating tool, see <https://cookiecutter.readthedocs.io>
